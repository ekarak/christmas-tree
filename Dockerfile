FROM ubuntu:18.04
# add dummy user, OpenCV and NodeJS
RUN useradd -G video -m foufoutos && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
        build-essential git curl \
        nodejs npm cmake pkg-config \
        libopencv-calib3d-dev libopencv-photo-dev \
        libopencv-contrib-dev libopencv-videoio-dev

# Fallback to dummy user and install nodeJS bindings
USER foufoutos
WORKDIR /home/foufoutos
RUN OPENCV4NODEJS_DISABLE_AUTOBUILD=1 OPENCV_LIB_DIR=/usr/lib/x86_64-linux-gnu npm install knx opencv4nodejs

COPY lib/ /home/foufoutos/christmas-tree/lib
CMD  'node /home/foufoutos/christmas-tree/lib/start.js'