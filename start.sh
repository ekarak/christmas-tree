#!/bin/bash

if ! [ -x "$(command -v x11docker)" ]; then
    wget -P /tmp https://raw.githubusercontent.com/mviereck/x11docker/master/x11docker
    cd /tmp && sudo bash ./x11docker install
fi

docker build . -t christmas-tree && \
x11docker --webcam --hostdisplay --verbose --user=RETAIN christmas-tree
